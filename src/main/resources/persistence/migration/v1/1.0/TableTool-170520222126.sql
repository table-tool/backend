UPDATE table_tool.games
SET photo = 'monopoly'
WHERE game_id = '224f8b0a-94df-470e-a03a-5446c3768dd1';

UPDATE table_tool.games
SET photo = 'uno'
WHERE game_id = '6c007733-c024-40db-a16c-eefcb6b095a7';

UPDATE table_tool.games
SET photo = 'jenga'
WHERE game_id = '2b68ee37-a09e-4f11-9572-ba99a8efb6f5';

UPDATE table_tool.games
SET photo = 'mtg'
WHERE game_id = '92f6de79-22ef-473f-a37d-37abc718e3fa';

UPDATE table_tool.games
SET photo = 'warhammer'
WHERE game_id = '00e25369-1320-4643-99b5-f938ac850bd6';