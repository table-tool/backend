create table if not exists table_tool.games_categories
(
    game_id  uuid not null
        constraint fkme3j7ysoses3yumlbfyv33sdn
            references table_tool.games,
    category_id uuid not null
        constraint fk3tuc300idwmuqtvlflc6pq037
            references table_tool.categories
);

truncate table_tool.meetings cascade ;

DELETE
FROM table_tool.games
WHERE game_id = '6c007733-c024-40db-a16c-eefcb6b095a7';

DELETE
FROM table_tool.games
WHERE game_id = '2b68ee37-a09e-4f11-9572-ba99a8efb6f5';

DELETE
FROM table_tool.games
WHERE game_id = '92f6de79-22ef-473f-a37d-37abc718e3fa';

DELETE
FROM table_tool.games
WHERE game_id = '00e25369-1320-4643-99b5-f938ac850bd6';

DELETE
FROM table_tool.games
WHERE game_id = '224f8b0a-94df-470e-a03a-5446c3768dd1';


alter table table_tool.games
    drop column category_id;

INSERT INTO table_tool.games (game_id, name) VALUES ('224f8b0a-94df-470e-a03a-5446c3768dd1', 'Monopoly');
INSERT INTO table_tool.games (game_id, name) VALUES ('6c007733-c024-40db-a16c-eefcb6b095a7', 'Uno');
INSERT INTO table_tool.games (game_id, name) VALUES ('00e25369-1320-4643-99b5-f938ac850bd6', 'Warhammer');
INSERT INTO table_tool.games (game_id, name) VALUES ('92f6de79-22ef-473f-a37d-37abc718e3fa', 'Magic: The Gathering');
INSERT INTO table_tool.games (game_id, name) VALUES ('2b68ee37-a09e-4f11-9572-ba99a8efb6f5', 'Jenga');

