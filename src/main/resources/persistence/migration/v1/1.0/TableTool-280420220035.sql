alter table table_tool.themes
    add theme_id uuid;

alter table table_tool.themes
    drop constraint themes_pk;

alter table table_tool.themes
    add constraint themes_pk
        primary key (theme_id);

