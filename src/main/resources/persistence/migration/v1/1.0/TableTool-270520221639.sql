alter table table_tool.products
    alter column price type numeric(12, 2) using price::numeric(12, 2);