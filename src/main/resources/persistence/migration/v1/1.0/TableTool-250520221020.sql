create table table_tool.products
(
    product_id   uuid,
    product_name varchar(255),
    price        int,
    link         varchar,
    photo_link   varchar,
    user_id      varchar
        constraint products_users_fk
            references table_tool.users
);

