alter table table_tool.users
    add external_id varchar(255);

truncate table_tool.users CASCADE;

alter table table_tool.users
    drop constraint users_pkey cascade;

alter table table_tool.users
    drop column user_id cascade;

alter table table_tool.users
    add username varchar(255);

create unique index users_username_uindex
    on table_tool.users (username);


alter table table_tool.users
    add constraint users_pk
        primary key (external_id);

alter table table_tool.users_ratings_meetings
    alter column evaluator_id type varchar(255) using evaluator_id::varchar(255);

alter table table_tool.users_ratings_meetings
    alter column user_id type varchar(255) using user_id::varchar(255);