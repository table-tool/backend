create schema if not exists table_tool;

create table if not exists table_tool.categories
(
    category_id uuid not null
        primary key,
    name        varchar(255)
);

create table if not exists table_tool.games
(
    game_id     uuid not null
        primary key,
    name        varchar(255),
    category_id uuid
        constraint fkgas7amj76041l6b8nqlx4mrvx
            references table_tool.categories
);

create table if not exists table_tool.users
(
    user_id uuid              not null
        primary key,
    cons    integer default 0 not null,
    pros    integer default 0 not null
);

create table if not exists table_tool.meetings
(
    meeting_id        uuid not null
        primary key,
    address           varchar(255),
    description       varchar(255),
    max_players_count integer default 1,
    period integer not null,
    start_date        timestamp,
    organizer_id      uuid
        constraint fkhoisnic8wo3fcpak12vig58pm
            references table_tool.users
);

create table if not exists table_tool.meetings_categories
(
    meeting_id  uuid not null
        constraint fkme3j7ysoses3yumlbfyv33sdn
            references table_tool.meetings,
    category_id uuid not null
        constraint fk3tuc300idwmuqtvlflc6pq037
            references table_tool.categories
);

create table if not exists table_tool.meetings_games
(
    meeting_id uuid not null
        constraint fkam0bcif0qkuio8eln2ue7j37d
            references table_tool.meetings,
    game_id    uuid not null
        constraint fkqxrsdjxem4sabhle2064fj3tm
            references table_tool.games
);

create table if not exists table_tool.users_meetings
(
    meeting_id uuid not null
        constraint fkmrxquq8pas0my6sf2jp2x6fwf
            references table_tool.meetings,
    user_id    uuid not null
        constraint fkpx4b9asxg1gsyheh1508p2hvd
            references table_tool.users
);

create table if not exists table_tool.users_ratings_meetings
(
    users_ratings_meetings_id uuid not null
        primary key,
    evaluator_id              uuid,
    meeting_id                uuid
        constraint fkpg7u9d16m6yjxpmx1fv5sh3vm
            references table_tool.meetings,
    rating_mark               integer,
    user_id                   uuid
);
