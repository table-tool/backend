create table table_tool.themes
(
    theme_type      varchar(255),
    background      varchar(255),
    item_background varchar(255),
    button_primary  varchar(255),
    separator_color varchar(255),
    white_element   varchar(255),
    black_element   varchar(255),
    color_secondary varchar(255),
    text_hint       varchar(255),
    settings_header varchar(255)
);

