INSERT INTO table_tool.themes (theme_type, background, item_background, button_primary, separator_color, white_element,
                               black_element, color_secondary, text_hint, settings_header, theme_id)
VALUES ('dark', '#FF070D22', '#FF0C1931', '#FF2879FA', '#FF3E508E', '#FFFFFFFF', '#FF000000', '#FFB0B0B0', '#FF505050',
        '#FF658CC8', 'b1a810d6-61d6-48de-9bed-deee50f2f5bb');

