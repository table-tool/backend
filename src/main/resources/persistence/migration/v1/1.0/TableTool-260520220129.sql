alter table table_tool.product_tags
    rename column description to name;

alter table table_tool.product_tags
    drop column name_class_name;

alter table table_tool.product_tags
    drop column name_page;

