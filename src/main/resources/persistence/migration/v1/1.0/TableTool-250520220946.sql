create table table_tool.product_tag
(
    product_tag_id  uuid not null primary key,
    class_name      varchar(255),
    description     varchar(255),
    price           varchar(255),
    name_class_name varchar(255),
    name_page       varchar(255)
);