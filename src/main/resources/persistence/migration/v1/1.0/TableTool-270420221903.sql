alter table table_tool.users_meetings
    alter column user_id type varchar(255) using user_id::varchar(255);

alter table table_tool.users
    rename column external_id to id;

