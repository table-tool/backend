alter table table_tool.meetings
    add constraint meetings_users_fk
        foreign key (organizer_id) references table_tool.users;

alter table table_tool.users_meetings
    add constraint users_meetings_users_fk
        foreign key (user_id) references table_tool.users;

alter table table_tool.users_ratings_meetings
    add constraint users_ratings_meetings_users_evaluator_id_fk
        foreign key (evaluator_id) references table_tool.users;

alter table table_tool.users_ratings_meetings
    add constraint users_ratings_meetings_users_user_id_fk
        foreign key (user_id) references table_tool.users;
