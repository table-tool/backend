package com.example.tabletool.service.logic.impl;

import com.example.tabletool.persistence.entity.CategoryEnum;
import com.example.tabletool.persistence.repository.GameRepository;
import com.example.tabletool.service.logic.GameService;
import com.example.tabletool.service.model.GameDto;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
@AllArgsConstructor
public class GameServiceImpl implements GameService {

  private final GameRepository gameRepository;

  @Override
  public List<GameDto> getAllGames() {
    return gameRepository.findAll()
      .stream()
      .map(g -> {
        GameDto gameDto = new GameDto();
        gameDto.setName(g.getName());
        gameDto.setImage(getResource(g.getPhoto()));

        return gameDto;
      })
      .toList();
  }

  @Override
  public List<GameDto> getAllGamesByCategory(CategoryEnum category) {
    return gameRepository.findAllByCategories_nameEquals(category)
      .stream()
      .map(g -> {
        GameDto gameDto = new GameDto();
        gameDto.setName(g.getName());
        gameDto.setImage(getResource(g.getPhoto()));

        return gameDto;
      })
      .toList();
  }

  @SneakyThrows
  private String getResource(String name) {
    Path path = Paths.get(name);
    byte[] bytes = Base64.encodeBase64(Files.readAllBytes(path));
    return new String(bytes, StandardCharsets.UTF_8);
  }
}
