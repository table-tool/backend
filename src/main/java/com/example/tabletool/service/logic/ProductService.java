package com.example.tabletool.service.logic;

import com.example.tabletool.service.model.ProductDto;
import com.example.tabletool.service.model.TrackedProductDto;

import javax.validation.Valid;
import java.util.List;

public interface ProductService {

  List<ProductDto> getProducts(String search);

  ProductDto addToTrackedList(@Valid TrackedProductDto productDto);

  List<ProductDto> getTrackedList();
}
