package com.example.tabletool.service.logic.impl;

import com.example.tabletool.persistence.entity.User;
import com.example.tabletool.persistence.repository.UserRepository;
import com.example.tabletool.service.logic.UserService;
import com.example.tabletool.service.mapper.UserMapper;
import com.example.tabletool.service.model.UserCreateDto;
import com.example.tabletool.service.model.UserDto;
import com.example.tabletool.service.model.UserUpdateDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  private final UserMapper userMapper;

  @Override
  public UserDto create(UserCreateDto userCreateDto) {

    User user = new User();
    user.setId(userCreateDto.getId());
    user.setUsername(userCreateDto.getUsername());
    user.setPros(5);
    user.setCons(5);

    User newUser = userRepository.save(user);

    return userMapper.fromEntity(newUser);
  }

  @Override
  public UserDto update(UserUpdateDto userUpdateDto) {

    User user = new User();
    user.setId(userUpdateDto.getId());
    user.setUsername(userUpdateDto.getUsername());

    User newUser = userRepository.save(user);

    return userMapper.fromEntity(newUser);
  }

  @Override
  public UserDto getById(String userId) {
    return userMapper.fromEntity(userRepository.getReferenceById(userId));
  }

  @Override
  public boolean checkValid(String username) {
    return !userRepository.existsByUsername(username);
  }
}
