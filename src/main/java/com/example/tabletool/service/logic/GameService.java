package com.example.tabletool.service.logic;

import com.example.tabletool.persistence.entity.CategoryEnum;
import com.example.tabletool.service.model.GameDto;

import java.util.List;

public interface GameService {

  List<GameDto> getAllGames();

  List<GameDto> getAllGamesByCategory(CategoryEnum category);
}
