package com.example.tabletool.service.logic.impl;

import com.example.tabletool.persistence.entity.ThemeType;
import com.example.tabletool.persistence.repository.ThemeRepository;
import com.example.tabletool.service.logic.ThemeService;
import com.example.tabletool.service.mapper.ThemeMapper;
import com.example.tabletool.service.model.ThemeDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class ThemeServiceImpl implements ThemeService {

  private final ThemeRepository themeRepository;

  private final ThemeMapper themeMapper;

  @Override
  public ThemeDto getThemeByType(ThemeType themeType) {
    return themeMapper.fromEntity(themeRepository.findByThemeType(themeType));
  }

  @Override
  public List<ThemeDto> getAllThemes() {
    return themeRepository.findAll()
      .stream()
      .map(themeMapper::fromEntity)
      .toList();
  }
}
