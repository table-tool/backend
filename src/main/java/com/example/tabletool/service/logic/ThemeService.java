package com.example.tabletool.service.logic;

import com.example.tabletool.persistence.entity.ThemeType;
import com.example.tabletool.service.model.ThemeDto;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface ThemeService {

  ThemeDto getThemeByType(
    @NotNull(message = "Тема не может быть пустой")
      ThemeType themeName
  );

  List<ThemeDto> getAllThemes();
}
