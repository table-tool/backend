package com.example.tabletool.service.logic.impl;

import com.example.tabletool.controller.feign.ProductClient;
import com.example.tabletool.persistence.entity.ProductTag;
import com.example.tabletool.persistence.repository.ProductRepository;
import com.example.tabletool.persistence.repository.ProductTagRepository;
import com.example.tabletool.security.SecurityService;
import com.example.tabletool.service.exception.ApiRequestException;
import com.example.tabletool.service.logic.ProductService;
import com.example.tabletool.service.mapper.ProductMapper;
import com.example.tabletool.service.model.ProductDto;
import com.example.tabletool.service.model.TrackedProductDto;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Validated
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

  private final ProductTagRepository productTagRepository;
  private final ProductRepository productRepository;
  private final SecurityService securityService;
  private final ProductMapper productMapper;
  private final ProductClient productClient;
  @Value("${owner.id}")
  private String ownerId;

  @Override
  @SneakyThrows
  public List<ProductDto> getProducts(String search) {
    List<ProductTag> all = productTagRepository.findAll();

    if (all.size() == 1) {

      ProductTag tag = all.get(0);

      for (int i = 0; i < 4; i++) {
        List<ProductDto> items = productClient
          .getProducts(search, tag.getClassName(), tag.getName().replace("\\", ""), tag.getPrice())
          .getItems();

        if (items != null && items.size() != 0) {
          return items;
        }
      }
    }

    return productRepository.findAllByOwnerId(ownerId)
      .stream()
      .map(productMapper::fromEntity)
      .toList();
  }

  @Override
  public ProductDto addToTrackedList(TrackedProductDto productDto) {

    if (!securityService.getUserId().equals(productDto.getUserId())) {
      throw new ApiRequestException("Добавить в отслеживаемые можно только товар для самого себя");
    }

    return Optional.of(productDto)
      .map(productMapper::toEntity)
      .map(productRepository::save)
      .map(productMapper::fromEntity)
      .orElse(null);
  }

  @Override
  public List<ProductDto> getTrackedList() {

    return productRepository.findAllByOwnerId(securityService.getUserId())
      .stream()
      .map(productMapper::fromEntity)
      .toList();
  }
}
