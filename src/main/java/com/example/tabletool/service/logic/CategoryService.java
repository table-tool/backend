package com.example.tabletool.service.logic;

import com.example.tabletool.persistence.entity.CategoryEnum;

import java.util.List;

public interface CategoryService {

  List<CategoryEnum> getAllCategories();
}
