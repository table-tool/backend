package com.example.tabletool.service.logic.impl;

import com.example.tabletool.persistence.entity.CategoryEnum;
import com.example.tabletool.service.logic.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CategoryServiceImpl implements CategoryService {

  @Override
  public List<CategoryEnum> getAllCategories() {
    return List.of(CategoryEnum.values());
  }
}
