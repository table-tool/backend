package com.example.tabletool.service.logic.impl;

import com.example.tabletool.persistence.entity.Meeting;
import com.example.tabletool.persistence.entity.User;
import com.example.tabletool.persistence.repository.GameRepository;
import com.example.tabletool.persistence.repository.MeetingRepository;
import com.example.tabletool.persistence.repository.UserRepository;
import com.example.tabletool.persistence.specification.meeting.MeetingFilterSpecification;
import com.example.tabletool.persistence.specification.meeting.MeetingSearchSpecification;
import com.example.tabletool.persistence.specification.meeting.MeetingSortSpecification;
import com.example.tabletool.security.SecurityService;
import com.example.tabletool.service.exception.ApiRequestException;
import com.example.tabletool.service.logic.MeetingService;
import com.example.tabletool.service.mapper.MeetingMapper;
import com.example.tabletool.service.model.*;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class MeetingServiceImpl implements MeetingService {

  private final MeetingRepository meetingRepository;
  private final UserRepository userRepository;

  private final GameRepository gameRepository;
  private final MeetingMapper meetingMapper;
  private final SecurityService securityService;

  @Override
  public MeetingPageDto getMeetings(MeetingSearchDto dto) {

    if (dto.getUserId() != null)
      dto.setUser(userRepository.getReferenceById(dto.getUserId()));

    if (dto.getSearchString() != null)
      dto.setGamesFromSearchString(gameRepository.searchByName(dto.getSearchString().toUpperCase()));

    var filterSpecification = new MeetingFilterSpecification(dto);
    var searchSpecification = new MeetingSearchSpecification(dto.getSearchString(), dto.getGamesFromSearchString());
    var sortSpecification = new MeetingSortSpecification(dto.getSortType(), dto.getSortOrder());

    Page<MeetingDto> leagues = meetingRepository
      .findAll(
        searchSpecification.and(filterSpecification).and(sortSpecification),
        PageRequest.of(dto.getPageNumber(), dto.getPageSize())
      )
      .map(meetingMapper::fromEntity);

    return ImmutableMeetingPageDto.builder()
      .pageNumber(dto.getPageNumber())
      .pageSize(dto.getPageSize())
      .totalElements(leagues.getTotalElements())
      .items(leagues.getContent())
      .build();
  }

  @Override
  public MeetingDto getMeetingById(UUID meetingId) {
    return meetingMapper.fromEntity(meetingRepository.getReferenceById(meetingId));
  }

  @Override
  public MeetingDto create(MeetingCreateDto dto) {
    return Optional.of(dto)
      .map(meetingMapper::toEntity)
      .map(meetingRepository::save)
      .map(meetingMapper::fromEntity)
      .orElse(null);
  }

  @Override
  public MeetingDto update(MeetingUpdateDto dto) {
    Meeting meeting = meetingRepository.getReferenceById(dto.getId());

    if (!securityService.getUserId().equals(meeting.getOrganizer().getId())) {
      throw new ApiRequestException("Вы не являетесь создателем данной встречи");
    }

    if (meeting.getMembers().size() > dto.getMaxPlayersCount())
      throw new ApiRequestException("Обновлённое максимальное число участников меньше, чем текущее количество участников");

    return Optional.of(dto)
      .map(meetingMapper::toEntity)
      .map(meetingRepository::save)
      .map(meetingMapper::fromEntity)
      .orElse(null);
  }

  @Override
  public void delete(UUID meetingId) {
    meetingRepository.deleteById(meetingId);
  }

  @Override
  public void addUserToMeeting(UUID meetingId, String userId) {

    if (!securityService.getUserId().equals(userId)) {
      throw new ApiRequestException("Добавить к встрече можно только самого себя");
    }

    Meeting meeting = meetingRepository.getReferenceById(meetingId);
    User user = userRepository.getReferenceById(userId);

    List<User> members = meeting.getMembers();
    if (!members.contains(user)) {
      members.add(user);
    } else {
      throw new ApiRequestException("Пользователь уже присутствует во встрече");
    }
  }

  @Override
  public void deleteUserFromMeeting(UUID meetingId, String userId) {
    Meeting meeting = meetingRepository.getReferenceById(meetingId);

    if (!securityService.getUserId().equals(meeting.getOrganizer().getId())) {
      throw new ApiRequestException("Вы не являетесь создателем данной встречи");
    }

    User user = userRepository.getReferenceById(userId);


    if (meeting.getOrganizer().equals(user)) {
      throw new ApiRequestException("Владельца исключить нельзя");
    }

    List<User> members = meeting.getMembers();
    if (members.contains(user)) {
      members.remove(user);
    } else {
      throw new ApiRequestException("Пользователь не является участником данной встречи");
    }
  }
}
