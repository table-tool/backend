package com.example.tabletool.service.logic;

import com.example.tabletool.service.model.UserCreateDto;
import com.example.tabletool.service.model.UserDto;
import com.example.tabletool.service.model.UserUpdateDto;
import com.example.tabletool.service.validation.UserIdExists;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface UserService {

  UserDto getById(@UserIdExists String userId);

  boolean checkValid(
    @NotNull(message = "Имя пользователя не может быть пустым")
      String username
  );

  UserDto create(@Valid UserCreateDto userCreateDto);

  UserDto update(@Valid UserUpdateDto userUpdateDto);
}
