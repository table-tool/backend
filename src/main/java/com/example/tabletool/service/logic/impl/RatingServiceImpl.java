package com.example.tabletool.service.logic.impl;

import com.example.tabletool.persistence.entity.Meeting;
import com.example.tabletool.persistence.entity.RatingMark;
import com.example.tabletool.persistence.entity.User;
import com.example.tabletool.persistence.entity.UserRating;
import com.example.tabletool.persistence.repository.MeetingRepository;
import com.example.tabletool.persistence.repository.UserRatingRepository;
import com.example.tabletool.persistence.repository.UserRepository;
import com.example.tabletool.service.exception.ApiRequestException;
import com.example.tabletool.service.logic.RatingService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
@Validated
@AllArgsConstructor
public class RatingServiceImpl implements RatingService {

  private final MeetingRepository meetingRepository;

  private final UserRatingRepository userRatingRepository;

  private final UserRepository userRepository;

  @Override
  public void ratePlayerInMeeting(UUID meetingId, String playerId, RatingMark rate, String thisPlayerId) {

    Meeting meeting = meetingRepository.getReferenceById(meetingId);
    User player = userRepository.getReferenceById(playerId);
    User evaluator = userRepository.getReferenceById(thisPlayerId);

    if (!meeting.getMembers().contains(evaluator)) {
      throw new ApiRequestException("Оценивающий участник не является членом заданной встречи");
    }

    if (!meeting.getMembers().contains(player)) {
      throw new ApiRequestException("Оцениваемый участник не является членом заданной встречи");
    }

    List<UserRating> userRatings = meeting.getUsersRating()
      .stream()
      .filter(ur -> ur.getEvaluatorId().equals(thisPlayerId) && ur.getUserId().equals(playerId))
      .toList();

    if (userRatings.isEmpty()) {
      UserRating userRating = new UserRating();

      userRating.setMeetingId(meetingId);
      userRating.setEvaluatorId(thisPlayerId);
      userRating.setUserId(playerId);
      userRating.setRatingMark(rate);

      player.addRating(rate);

      userRatingRepository.save(userRating);
      userRepository.save(player);
    }

    if (userRatings.size() == 1) {
      UserRating userRating = userRatings.get(0);

      player.reduceRating(userRating.getRatingMark());

      userRating.setRatingMark(rate);

      player.addRating(rate);

      userRatingRepository.save(userRating);
      userRepository.save(player);
    }
  }

  @Override
  public void unratePlayerInMeeting(UUID meetingId, String playerId, String thisPlayerId) {

    Meeting meeting = meetingRepository.getReferenceById(meetingId);
    User player = userRepository.getReferenceById(playerId);

    List<UserRating> userRatings = meeting.getUsersRating()
      .stream()
      .filter(ur -> ur.getEvaluatorId().equals(thisPlayerId) && ur.getUserId().equals(playerId))
      .toList();

    if (userRatings.size() == 1) {
      UserRating userRating = userRatings.get(0);

      player.reduceRating(userRating.getRatingMark());

      userRatingRepository.delete(userRating);
      userRepository.save(player);
    }
  }
}
