package com.example.tabletool.service.logic;

import com.example.tabletool.persistence.entity.RatingMark;
import com.example.tabletool.service.validation.MeetingExists;
import com.example.tabletool.service.validation.UserIdExists;

import java.util.UUID;

public interface RatingService {

  void ratePlayerInMeeting(
    @MeetingExists UUID meetingId,
    @UserIdExists String playerId,
    RatingMark rate,
    @UserIdExists String thisPlayerId
  );

  void unratePlayerInMeeting(
    @MeetingExists UUID meetingId,
    @UserIdExists String playerId,
    @UserIdExists String thisPlayerId
  );
}
