package com.example.tabletool.service.logic;

import com.example.tabletool.service.model.*;
import com.example.tabletool.service.validation.MeetingExists;
import com.example.tabletool.service.validation.UserIdExists;

import javax.validation.Valid;
import java.util.UUID;

public interface MeetingService {

  MeetingPageDto getMeetings(@Valid MeetingSearchDto dto);

  MeetingDto getMeetingById(@MeetingExists UUID meetingId);

  MeetingDto create(@Valid MeetingCreateDto dto);

  MeetingDto update(@Valid MeetingUpdateDto dto);

  void delete(@MeetingExists UUID meetingId);

  void addUserToMeeting(@MeetingExists UUID meetingId, @UserIdExists String userId);

  void deleteUserFromMeeting(@MeetingExists UUID meetingId, @UserIdExists String userId);
}
