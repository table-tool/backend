package com.example.tabletool.service.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "Ответ в случае ошибки")
public record ExceptionDto(
  @Schema(description = "Список ошибок")
  List<String> messages,
  @Schema(defaultValue = "400 BAD_REQUEST")
  @JsonProperty("http_status")
  HttpStatus httpStatus,
  LocalDateTime time
) {
}
