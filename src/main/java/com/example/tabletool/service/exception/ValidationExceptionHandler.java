package com.example.tabletool.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class ValidationExceptionHandler {

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ExceptionDto> handleAnv(MethodArgumentNotValidException e) {
    HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

    List<String> errors = new ArrayList<>();

    errors.addAll(e.getBindingResult().getFieldErrors().stream()
      .map(FieldError::getDefaultMessage)
      .toList());

    errors.addAll(e.getBindingResult().getGlobalErrors().stream()
      .map(ObjectError::getDefaultMessage)
      .toList());

    ExceptionDto exceptionDto = new ExceptionDto(
      errors,
      httpStatus,
      LocalDateTime.now()
    );

    return ResponseEntity.status(httpStatus).body(exceptionDto);
  }

  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<ExceptionDto> handleAnv(ConstraintViolationException cve) {
    HttpStatus httpStatus = HttpStatus.BAD_REQUEST;


    List<String> errors = new ArrayList<>(
      cve.getConstraintViolations().stream()
        .map(ConstraintViolation::getMessage)
        .toList()
    );

    ExceptionDto exceptionDto = new ExceptionDto(
      errors,
      httpStatus,
      LocalDateTime.now()
    );

    return ResponseEntity.status(httpStatus).body(exceptionDto);
  }
}
