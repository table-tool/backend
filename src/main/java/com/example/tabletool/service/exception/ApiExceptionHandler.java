package com.example.tabletool.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.List;

@ControllerAdvice
public class ApiExceptionHandler {

  @ExceptionHandler(ApiRequestException.class)
  public ResponseEntity<ExceptionDto> handleApiRequestException(ApiRequestException e) {
    HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

    ExceptionDto exceptionDto = new ExceptionDto(
      List.of(e.getMessage()),
      httpStatus,
      LocalDateTime.now()
    );

    return new ResponseEntity<>(exceptionDto, httpStatus);
  }
}
