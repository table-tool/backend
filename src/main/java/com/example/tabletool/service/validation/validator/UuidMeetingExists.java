package com.example.tabletool.service.validation.validator;

import com.example.tabletool.persistence.repository.MeetingRepository;
import com.example.tabletool.service.validation.MeetingExists;
import lombok.AllArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

@AllArgsConstructor
public class UuidMeetingExists implements ConstraintValidator<MeetingExists, UUID> {

  private final MeetingRepository meetingRepository;

  @Override
  public boolean isValid(UUID id, ConstraintValidatorContext context) {
    if (id == null) {
      return true;
    }

    return meetingRepository.existsById(id);
  }
}
