package com.example.tabletool.service.validation;

import com.example.tabletool.service.validation.validator.StringUserDoesNotExist;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Documented
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StringUserDoesNotExist.class)
public @interface UserIdDoesNotExist {

  String message() default "Пользователь с данным id уже существует";

  Class<? extends Payload>[] payload() default {};

  Class<?>[] groups() default {};
}
