package com.example.tabletool.service.validation;

import com.example.tabletool.service.validation.validator.StringUsernameDoesNotExist;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Documented
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StringUsernameDoesNotExist.class)
public @interface UsernameDoesNotExist {

  String message() default "Пользователь с данным именем уже существует";

  Class<? extends Payload>[] payload() default {};

  Class<?>[] groups() default {};
}
