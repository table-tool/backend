package com.example.tabletool.service.validation;

import com.example.tabletool.service.validation.validator.UuidMeetingExists;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Documented
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UuidMeetingExists.class)
public @interface MeetingExists {

  String message() default "Встреча с данным id не существует";

  Class<? extends Payload>[] payload() default {};

  Class<?>[] groups() default {};
}
