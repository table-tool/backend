package com.example.tabletool.service.validation.validator;

import com.example.tabletool.persistence.repository.UserRepository;
import com.example.tabletool.service.validation.UsernameDoesNotExist;
import lombok.AllArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@AllArgsConstructor
public class StringUsernameDoesNotExist implements ConstraintValidator<UsernameDoesNotExist, String> {

  private final UserRepository userRepository;

  @Override
  public boolean isValid(String id, ConstraintValidatorContext context) {
    if (id == null) {
      return true;
    }

    return !userRepository.existsByUsername(id);
  }
}
