package com.example.tabletool.service.validation.validator;

import com.example.tabletool.persistence.repository.UserRepository;
import com.example.tabletool.service.validation.UserIdDoesNotExist;
import lombok.AllArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@AllArgsConstructor
public class StringUserDoesNotExist implements ConstraintValidator<UserIdDoesNotExist, String> {

  private final UserRepository userRepository;

  @Override
  public boolean isValid(String id, ConstraintValidatorContext context) {
    if (id == null) {
      return true;
    }

    return !userRepository.existsById(id);
  }
}
