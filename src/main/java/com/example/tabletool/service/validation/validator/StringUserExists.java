package com.example.tabletool.service.validation.validator;

import com.example.tabletool.persistence.repository.UserRepository;
import com.example.tabletool.service.validation.UserIdExists;
import lombok.AllArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@AllArgsConstructor
public class StringUserExists implements ConstraintValidator<UserIdExists, String> {

  private final UserRepository userRepository;

  @Override
  public boolean isValid(String id, ConstraintValidatorContext context) {
    if (id == null) {
      return true;
    }

    return userRepository.existsById(id);
  }
}
