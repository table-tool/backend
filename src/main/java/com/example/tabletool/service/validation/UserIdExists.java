package com.example.tabletool.service.validation;

import com.example.tabletool.service.validation.validator.StringUserExists;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Documented
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StringUserExists.class)
public @interface UserIdExists {

  String message() default "Пользователь с данным id не существует";

  Class<? extends Payload>[] payload() default {};

  Class<?>[] groups() default {};
}
