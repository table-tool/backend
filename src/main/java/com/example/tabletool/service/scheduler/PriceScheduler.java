package com.example.tabletool.service.scheduler;

import com.example.tabletool.controller.feign.ProductClient;
import com.example.tabletool.persistence.entity.Product;
import com.example.tabletool.persistence.entity.ProductTag;
import com.example.tabletool.persistence.repository.ProductRepository;
import com.example.tabletool.persistence.repository.ProductTagRepository;
import com.example.tabletool.service.model.PriceDto;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
@Slf4j
@RequiredArgsConstructor
public class PriceScheduler {
  private final ProductRepository productRepository;
  private final ProductTagRepository productTagRepository;
  private final ProductClient productClient;

  @Value("${owner.id}")
  private String ownerId;
  @Value("${owner.int}")
  private String ownerInt;

  @Scheduled(cron = "@midnight")
  public void priceNotification() {

    List<ProductTag> all = productTagRepository.findAll();

    if (all.size() == 1) {
      ProductTag tag = all.get(0);

      List<Product> products = productRepository
        .findAll()
        .stream()
        .filter(p -> !p.getOwnerId().equals(ownerId)).toList();

      for (Product product : products) {
        for (int i = 0; i < 4; i++) {
          PriceDto newPriceDto;

          try {
            newPriceDto = productClient.getPrice(product.getLink(), tag.getLink());
          } catch (Exception e) {
            break;
          }

          BigDecimal ownerBigDecimal = new BigDecimal(ownerInt);
          BigDecimal newPrice = newPriceDto.getPrice();
          BigDecimal oldPrice = product.getPrice();

          if (!newPrice.equals(ownerBigDecimal) && newPrice.compareTo(oldPrice) < 0) {

            Message message = Message.builder()
              .setNotification(Notification.builder()
                .setTitle("Цена на товар из отслеживаемых изменилась")
                .build())
              .putData("old_price", oldPrice.toString())
              .putData("new_price", newPrice.toString())
              .putData("product_name", product.getName())
              .setTopic(product.getOwnerId())
              .build();

            product.setPrice(newPrice);

            productRepository.save(product);

            try {
              FirebaseMessaging.getInstance().send(message);
            } catch (FirebaseMessagingException e) {
              log.warn("Fail to send message");
            }

            break;
          }
        }
      }
    }
  }
}
