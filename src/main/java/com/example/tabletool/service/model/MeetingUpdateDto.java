package com.example.tabletool.service.model;

import com.example.tabletool.service.validation.MeetingExists;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
@Schema(description = "Структура для обновления встречи")
public class MeetingUpdateDto extends MeetingCreateDto {

  @Schema(description = "id встречи", example = "48668183-fdc8-43b2-86c2-003b520f7e66")
  @MeetingExists
  @NotNull(message = "id встречи не может быть пустым")
  @JsonProperty("meeting_id")
  private UUID id;
}
