package com.example.tabletool.service.model;

import com.example.tabletool.service.validation.UserIdDoesNotExist;
import com.example.tabletool.service.validation.UsernameDoesNotExist;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@Schema(description = "Структура для создания пользователя")
public class UserCreateDto {

  @NotNull(message = "id пользователя не может быть пустым")
  @JsonProperty("user_id")
  @UserIdDoesNotExist
  private String id;

  @NotNull(message = "Имя пользователя не может быть пустым")
  @UsernameDoesNotExist
  @Size(min = 2, max = 15, message = "Длина имени пользователя может быть от 2 до 15 символов")
  @Pattern(regexp = "[a-zA-Z]*", message = "Имя пользователя может содержать только буквы латинского алфавита")
  private String username;
}
