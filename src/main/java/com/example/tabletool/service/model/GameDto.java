package com.example.tabletool.service.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema(description = "Структура игры")
public class GameDto {

  private String name;

  private String image;
}
