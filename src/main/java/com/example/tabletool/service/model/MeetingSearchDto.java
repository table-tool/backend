package com.example.tabletool.service.model;

import com.example.tabletool.persistence.entity.*;
import com.example.tabletool.service.validation.UserIdExists;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MeetingSearchDto {

  private Integer pageNumber;

  private Integer pageSize;

  private SortOrder sortOrder;

  private SortType sortType;

  private String searchString;

  @UserIdExists
  private String userId;

  @UserIdExists
  private String organizerId;

  private User user;

  private MeetingStatus meetingStatus;

  private String address;

  private List<Category> categories;

  private List<Game> games;

  private RatingState ratingStateStart;

  private RatingState ratingStateEnd;

  private Integer periodStart;

  private Integer periodEnd;

  private Integer membersCountStart;

  private Integer membersCountEnd;

  private Integer maxMembersCountStart;

  private Integer maxMembersCountEnd;

  private List<Game> gamesFromSearchString;
}
