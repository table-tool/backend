package com.example.tabletool.service.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@Schema(description = "Структура товара в магазине")
public class ProductDto {

  @NotNull(message = "Имя товара не может быть пустым")
  private String name;

  @NotNull(message = "Цена товара не может быть пустой")
  private BigDecimal price;

  @NotNull(message = "Ссылка на товар не может быть пустой")
  private String link;

  @NotNull(message = "Ссылка на фото товара не может быть пустой")
  @JsonProperty("photo_link")
  private String photoLink;
}
