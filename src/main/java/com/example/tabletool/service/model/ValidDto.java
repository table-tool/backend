package com.example.tabletool.service.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidDto {

  private Boolean valid;
}
