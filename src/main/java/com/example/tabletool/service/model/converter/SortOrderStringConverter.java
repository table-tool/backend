package com.example.tabletool.service.model.converter;

import com.example.tabletool.persistence.entity.SortOrder;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class SortOrderStringConverter implements Converter<String, SortOrder> {

  @Override
  public SortOrder convert(@NonNull String name) {
    return SortOrder.fromName(name).orElse(null);
  }
}
