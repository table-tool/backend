package com.example.tabletool.service.model;

import com.example.tabletool.persistence.entity.CategoryEnum;
import com.example.tabletool.persistence.entity.RatingState;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Schema(description = "Структура встречи")
public class MeetingDto extends MeetingUpdateDto {

  @JsonProperty("organizer_rating")
  private RatingState organizerRating;

  @Schema(description = "Участники")
  private List<UserDto> members;

  @Schema(description = "Оценки пользователей")
  @JsonProperty("users_rating")
  private List<RatingDto> usersRating;

  private List<CategoryEnum> categories;
}
