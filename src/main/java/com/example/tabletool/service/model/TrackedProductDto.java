package com.example.tabletool.service.model;

import com.example.tabletool.service.validation.UserIdExists;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Schema(description = "Структура для добавления товара в список отслеживаемых")
public class TrackedProductDto extends ProductDto {

  @Schema(description = "id пользователя")
  @UserIdExists
  @NotNull(message = "id пользователя не может быть пустым")
  @JsonProperty("user_id")
  private String userId;

  public TrackedProductDto(ProductDto dto, String userId) {
    setUserId(userId);
    setName( dto.getName() );
    setPrice( dto.getPrice() );
    setLink( dto.getLink() );
    setPhotoLink( dto.getPhotoLink() );
  }

  public TrackedProductDto() {
  }
}
