package com.example.tabletool.service.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import org.immutables.value.Value;

import java.util.List;

@Schema(description = "Постраничная выдача")
@Value.Immutable
@Value.Style(privateNoargConstructor = true)
@JsonSerialize(as = ImmutableMeetingPageDto.class)
@JsonDeserialize(as = ImmutableMeetingPageDto.class)
public interface MeetingPageDto {

  static MeetingPageDto empty() {
    return ImmutableMeetingPageDto
      .builder()
      .items(List.of())
      .pageNumber(0)
      .pageSize(0)
      .build();
  }

  @Schema(description = "Элементы")
  List<MeetingDto> getItems();

  @Schema(description = "Номер страницы")
  @JsonProperty("page_number")
  int getPageNumber();

  @Schema(description = "Количество элементов на странице")
  @JsonProperty("page_size")
  int getPageSize();

  @Schema(description = "Всего элементов")
  @JsonProperty("total_elements")
  long getTotalElements();
}
