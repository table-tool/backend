package com.example.tabletool.service.model;

import com.example.tabletool.service.validation.UserIdExists;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Schema(description = "Структура для создания встречи")
public class MeetingCreateDto {

  @NotNull(message = "Дата начала не может быть пустой")
  @Future(message = "Дата начала находится в прошлом")
  @JsonProperty("start_date")
  @Schema(description = "Дата начала", example = "2022-05-27T14:09", format = "date", pattern = "yyyy-mm-ddThh:mm")
  private LocalDateTime startDate;

  @Schema(description = "Адрес")
  @NotEmpty(message = "Адрес не может быть пустым")
  private String address;

  @Schema(description = "Описание")
  @NotEmpty(message = "Описание не может быть пустым")
  private String description;

  @Schema(description = "Максимальное число участников")
  @NotNull(message = "Максимальное число участников не может быть пустым")
  @Max(value = 20, message = "Максимальное число участников должно быть меньше 20")
  @Min(value = 2, message = "Максимальное число участников должно быть больше 1")
  @JsonProperty("max_players_count")
  private Integer maxPlayersCount;

  @Schema(description = "Продолжительность (минуты)")
  @NotNull(message = "Продолжительность не может быть пустой")
  @Max(value = 600, message = "Продолжительность не может быть больше 600 минут")
  @Min(value = 30, message = "Продолжительность не может быть меньше 30 минут")
  private Integer period;

  @Schema(description = "id организатора")
  @UserIdExists(message = "id организатора не существует")
  @NotNull(message = "id организатора не может быть пустым")
  @JsonProperty("organizer_id")
  private String organizerId;

  @Schema(description = "id чата")
  @NotNull(message = "id чата не может быть пустым")
  @JsonProperty("chat_id")
  private String chatId;

  @NotEmpty(message = "Встреча должна иметь игру")
  private List<String> games;
}
