package com.example.tabletool.service.model.converter;

import com.example.tabletool.persistence.entity.ThemeType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class ThemeTypeStringConverter implements Converter<String, ThemeType> {

  @Override
  public ThemeType convert(@NonNull String name) {
    return ThemeType.fromName(name).orElse(null);
  }
}
