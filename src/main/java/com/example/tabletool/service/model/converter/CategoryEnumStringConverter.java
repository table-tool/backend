package com.example.tabletool.service.model.converter;

import com.example.tabletool.persistence.entity.CategoryEnum;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class CategoryEnumStringConverter implements Converter<String, CategoryEnum> {

  @Override
  public CategoryEnum convert(@NonNull String name) {
    return CategoryEnum.fromName(name).orElse(null);
  }
}
