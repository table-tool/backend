package com.example.tabletool.service.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Schema(description = "Структура списка товаров в магазине")
public class ProductDtoList {

  private List<ProductDto> items;
}
