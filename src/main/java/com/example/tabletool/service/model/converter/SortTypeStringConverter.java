package com.example.tabletool.service.model.converter;

import com.example.tabletool.persistence.entity.SortType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class SortTypeStringConverter implements Converter<String, SortType> {

  @Override
  public SortType convert(@NonNull String name) {
    return SortType.fromName(name).orElse(null);
  }
}
