package com.example.tabletool.service.model;

import com.example.tabletool.service.validation.UserIdExists;
import com.example.tabletool.service.validation.UsernameDoesNotExist;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Schema(description = "Структура для обновления пользователя")
public class UserUpdateDto {

  @NotNull(message = "id пользователя не может быть пустым")
  @JsonProperty("user_id")
  @UserIdExists
  private String id;

  @NotNull(message = "Имя пользователя не может быть пустым")
  @UsernameDoesNotExist
  private String username;
}
