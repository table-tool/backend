package com.example.tabletool.service.model;

import com.example.tabletool.persistence.entity.RatingState;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema(description = "Структура пользователя")
public class UserDto {

  @Schema(description = "id пользователя")
  @JsonProperty("user_id")
  private String id;

  @Schema(description = "Имя пользователя")
  private String username;

  @Schema(implementation = RatingState.class)
  private RatingState rating;
}
