package com.example.tabletool.service.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Schema(name = "Структура цены товара")
public class PriceDto {

  private BigDecimal price;
}
