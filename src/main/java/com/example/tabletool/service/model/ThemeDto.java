package com.example.tabletool.service.model;

import com.example.tabletool.persistence.entity.ThemeType;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema(description = "Структура темы")
public class ThemeDto {

  @JsonProperty("theme_type")
  private ThemeType themeType;

  @JsonProperty("background")
  private String background;
  @JsonProperty("item_background")
  private String itemBackground;
  @JsonProperty("button_primary")
  private String buttonPrimary;
  @JsonProperty("separator_color")
  private String separatorColor;
  @JsonProperty("white_element")
  private String whiteElement;
  @JsonProperty("black_element")
  private String blackElement;
  @JsonProperty("color_secondary")
  private String colorSecondary;
  @JsonProperty("text_hint")
  private String textHint;
  @JsonProperty("settings_header")
  private String settingsHeader;
}
