package com.example.tabletool.service.model.converter;

import com.example.tabletool.persistence.entity.MeetingStatus;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class MeetingStatusStringConverter implements Converter<String, MeetingStatus> {

  @Override
  public MeetingStatus convert(@NonNull String name) {
    return MeetingStatus.fromName(name).orElse(null);
  }
}
