package com.example.tabletool.service.model.converter;

import com.example.tabletool.persistence.entity.RatingMark;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class RatingMarkStringConverter implements Converter<String, RatingMark> {

  @Override
  public RatingMark convert(@NonNull String name) {
    return RatingMark.fromName(name).orElse(null);
  }
}
