package com.example.tabletool.service.model;

import com.example.tabletool.persistence.entity.RatingMark;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema(description = "Структура оценки пользователя пользователем")
public class RatingDto {

  @JsonProperty("user_id")
  @Schema(description = "id оцениваемого пользователя")
  private String userId;

  @JsonProperty("evaluator_id")
  @Schema(description = "id оценивающего пользователя")
  private String evaluatorId;

  @JsonProperty("rating_mark")
  @Schema(implementation = RatingMark.class)
  private RatingMark ratingMark;
}
