package com.example.tabletool.service.model.converter;

import com.example.tabletool.persistence.entity.RatingState;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class RatingStateStringConverter implements Converter<String, RatingState> {

  @Override
  public RatingState convert(@NonNull String name) {
    return RatingState.fromStringId(name).orElse(null);
  }
}
