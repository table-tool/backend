package com.example.tabletool.service.mapper;

import com.example.tabletool.persistence.entity.UserRating;
import com.example.tabletool.service.model.RatingDto;
import org.mapstruct.Mapper;

@Mapper
public abstract class UserRatingMapper {

  public abstract RatingDto fromEntity(UserRating ratingMeeting);
}
