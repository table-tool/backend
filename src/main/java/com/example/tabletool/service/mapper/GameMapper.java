package com.example.tabletool.service.mapper;

import com.example.tabletool.persistence.entity.Game;
import com.example.tabletool.persistence.repository.GameRepository;
import com.example.tabletool.service.exception.ApiRequestException;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Mapper
public abstract class GameMapper {

  @Autowired
  protected GameRepository gameRepository;

  public String fromEntity(Game game) {
    if (game == null) {
      return null;
    }

    return game.getName();
  }

  public List<Game> toEntities(List<String> names) {
    if (names == null) {
      return List.of();
    }

    List<Game> games = names
      .stream()
      .filter(Objects::nonNull)
      .distinct()
      .map(gameRepository::findByName)
      .filter(Optional::isPresent)
      .map(Optional::get)
      .toList();

    if (games.isEmpty()) {
      throw new ApiRequestException("У встречи должна быть игра");
    }

    return games;
  }
}
