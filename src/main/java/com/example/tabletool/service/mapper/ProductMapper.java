package com.example.tabletool.service.mapper;

import com.example.tabletool.persistence.entity.Product;
import com.example.tabletool.service.model.ProductDto;
import com.example.tabletool.service.model.TrackedProductDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public abstract class ProductMapper {

  public abstract ProductDto fromEntity(Product product);

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "ownerId", source = "userId")
  public abstract Product toEntity(TrackedProductDto dto);
}
