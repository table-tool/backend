package com.example.tabletool.service.mapper;

import com.example.tabletool.persistence.entity.Theme;
import com.example.tabletool.service.model.ThemeDto;
import org.mapstruct.Mapper;

@Mapper
public abstract class ThemeMapper {

  public abstract ThemeDto fromEntity(Theme theme);
}
