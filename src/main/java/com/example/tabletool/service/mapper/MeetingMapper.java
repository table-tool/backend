package com.example.tabletool.service.mapper;

import com.example.tabletool.persistence.entity.Meeting;
import com.example.tabletool.persistence.entity.User;
import com.example.tabletool.persistence.repository.MeetingRepository;
import com.example.tabletool.persistence.repository.UserRepository;
import com.example.tabletool.service.model.MeetingCreateDto;
import com.example.tabletool.service.model.MeetingDto;
import com.example.tabletool.service.model.MeetingUpdateDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Mapper(uses = {
  UserRatingMapper.class,
  UserMapper.class,
  CategoryMapper.class,
  GameMapper.class
})
public abstract class MeetingMapper {

  @Autowired
  protected UserRepository userRepository;

  @Autowired
  protected MeetingRepository meetingRepository;

  @Mapping(
    target = "organizerRating",
    expression = "java(meeting.getOrganizer().getRatingState())"
  )
  @Mapping(
    target = "organizerId",
    expression = "java(meeting.getOrganizer().getId())"
  )
  public abstract MeetingDto fromEntity(Meeting meeting);

  @Mapping(
    target = "usersRating",
    expression = "java(meetingRepository.getReferenceById(dto.getId()).getUsersRating())"
  )
  @Mapping(
    target = "organizer",
    expression = "java(userMapper.getUserById(dto.getOrganizerId()))"
  )
  @Mapping(
    target = "members",
    expression = "java(meetingRepository.getReferenceById(dto.getId()).getMembers())"
  )
  @Mapping(
    target = "games",
    expression = "java(gameMapper.toEntities(dto.getGames()))"
  )
  @Mapping(
    target = "categories",
    expression = "java(categoryMapper.toEntitiesFromGames(dto.getGames()))"
  )
  public abstract Meeting toEntity(MeetingUpdateDto dto);

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "usersRating", ignore = true)
  @Mapping(
    target = "organizer",
    expression = "java(userMapper.getUserById(dto.getOrganizerId()))"
  )
  @Mapping(
    target = "members",
    source = "organizerId", qualifiedByName = "addOrganizerToMeeting"
  )
  @Mapping(
    target = "games",
    expression = "java(gameMapper.toEntities(dto.getGames()))"
  )
  @Mapping(
    target = "categories",
    expression = "java(categoryMapper.toEntitiesFromGames(dto.getGames()))"
  )
  public abstract Meeting toEntity(MeetingCreateDto dto);

  @Named(value = "addOrganizerToMeeting")
  protected List<User> addOrganizerToMeeting(String organizerId) {
    return new ArrayList<>(List.of(userRepository.getReferenceById(organizerId)));
  }
}
