package com.example.tabletool.service.mapper;

import com.example.tabletool.persistence.entity.User;
import com.example.tabletool.persistence.repository.UserRepository;
import com.example.tabletool.service.model.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper
public abstract class UserMapper {

  @Autowired
  protected UserRepository userRepository;

  @Mapping(target = "rating", expression = "java(user.getRatingState())")
  public abstract UserDto fromEntity(User user);


  public User toEntity(UserDto dto) {
    return userRepository.getReferenceById(dto.getId());
  }

  public User getUserById(String userId) {
    return userRepository.getReferenceById(userId);
  }
}
