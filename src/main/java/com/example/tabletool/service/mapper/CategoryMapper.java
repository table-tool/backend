package com.example.tabletool.service.mapper;

import com.example.tabletool.persistence.entity.Category;
import com.example.tabletool.persistence.entity.CategoryEnum;
import com.example.tabletool.persistence.repository.CategoryRepository;
import com.example.tabletool.persistence.repository.GameRepository;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Mapper
public abstract class CategoryMapper {

  @Autowired
  protected CategoryRepository categoryRepository;
  @Autowired
  protected GameRepository gameRepository;

  public CategoryEnum fromEntity(Category category) {
    return category.getName();
  }

  public Category toEntity(CategoryEnum categoryEnum) {
    if (categoryEnum == null) {
      return null;
    }

    Category category = new Category();

    category.setId(categoryEnum.getUuid());
    category.setName(categoryEnum);

    return category;
  }

  public List<Category> toEntitiesFromGames(List<String> games) {
    if (games == null) {
      return List.of();
    }

    return games
      .stream()
      .filter(Objects::nonNull)
      .distinct()
      .map(gameRepository::findByName)
      .filter(Optional::isPresent)
      .map(Optional::get)
      .flatMap(game -> game.getCategories().stream())
      .toList();
  }

  public List<Category> toEntities(List<CategoryEnum> categories) {
    if (categories == null) {
      return List.of();
    }

    return categories
      .stream()
      .filter(Objects::nonNull)
      .distinct()
      .map(this::toEntity)
      .toList();
  }
}
