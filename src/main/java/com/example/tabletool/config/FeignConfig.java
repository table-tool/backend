package com.example.tabletool.config;

import com.example.tabletool.controller.feign.ProductClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(clients = {
  ProductClient.class,
})
public class FeignConfig {
}
