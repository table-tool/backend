package com.example.tabletool.controller;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.example.tabletool.controller.api")
public class RestConfiguration {
}
