package com.example.tabletool.controller.api;

import com.example.tabletool.persistence.entity.*;
import com.example.tabletool.service.exception.ExceptionDto;
import com.example.tabletool.service.logic.MeetingService;
import com.example.tabletool.service.mapper.CategoryMapper;
import com.example.tabletool.service.mapper.GameMapper;
import com.example.tabletool.service.model.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/meetings")
@AllArgsConstructor
@Tag(name = "meetings")
public class MeetingController {

  private final MeetingService meetingService;

  private final CategoryMapper categoryMapper;

  private final GameMapper gameMapper;

  @Operation(
    summary = "Получение списка встреч постранично с возможностью поиска, фильтрации и сортировки по критериям",
    security = {@SecurityRequirement(name = "bearer-key")}
  )
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Постраничная выдача встреч",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = MeetingPageDto.class)
        )}
      ),
    @ApiResponse(
      responseCode = "400",
      description = "Несуществующй id организатора встречи",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class)
        )}
    )}
  )
  @GetMapping
  MeetingPageDto getMeetings(
    @Parameter(description = "Номер страницы")
    @RequestParam(defaultValue = "0", name = "page_number")
      Integer pageNumber,
    @Parameter(description = "Количество элементов на странице")
    @RequestParam(defaultValue = "10", name = "page_size")
      Integer pageSize,
    @Parameter(description = "Строка, по которой выполняется поиск встреч")
    @RequestParam(required = false, name = "search_string")
      String searchString,
    @Parameter(description = "Фильтрация по прошедшим/предстоящим встречам")
    @Schema(implementation = MeetingStatus.class)
    @RequestParam(required = false, name = "meeting_status")
      MeetingStatus meetingStatus,
    @Parameter(description = "Фильтрация по встречам, в которых участвует пользователь с заданным id")
    @RequestParam(required = false, name = "user_id")
      String userId,
    @Parameter(description = "Фильтрация по встречам, в которых пользователь с заданным id является владельцем")
    @RequestParam(required = false, name = "organizer_id")
      String organizerId,
    @Parameter(description = "Фильтрация по заданным играм")
    @RequestParam(required = false, name = "games")
      List<String> games,
    @Parameter(description = "Фильтрация по заданным категориям")
    @RequestParam(required = false, name = "categories")
      List<CategoryEnum> categories,
    @Parameter(description = "Фильтрация по рейтингу организатора (левая граница)")
    @Schema(implementation = RatingState.class)
    @RequestParam(required = false, name = "rating_state_start")
      RatingState ratingStateStart,
    @Parameter(description = "Фильтрация по рейтингу организатора (правая граница)")
    @Schema(implementation = RatingState.class)
    @RequestParam(required = false, name = "rating_state_end")
      RatingState ratingStateEnd,
    @Parameter(description = "Фильтрация по адресу встречи")
    @RequestParam(required = false, name = "address")
      String address,
    @Parameter(description = "Фильтрация по продолжительности встречи (левая граница)")
    @RequestParam(required = false, name = "period_start")
      Integer periodStart,
    @Parameter(description = "Фильтрация по продолжительности встречи (правая граница)")
    @RequestParam(required = false, name = "period_end")
      Integer periodEnd,
    @Parameter(description = "Фильтрация по количеству участников (левая граница)")
    @RequestParam(required = false, name = "members_count_start")
      Integer membersCountStart,
    @Parameter(description = "Фильтрация по количеству участников (правая граница)")
    @RequestParam(required = false, name = "members_count_end")
      Integer membersCountEnd,
    @Parameter(description = "Фильтрация по максимальному количеству участников (левая граница)")
    @RequestParam(required = false, name = "max_members_count_start")
      Integer maxMembersCountStart,
    @Parameter(description = "Фильтрация по максимальному количеству участников (правая граница)")
    @RequestParam(required = false, name = "max_members_count_end")
      Integer maxMembersCountEnd,
    @Parameter(description = "Сортировка по заданному типу")
    @Schema(implementation = SortType.class)
    @RequestParam(required = false, name = "sort_type")
      SortType sortType,
    @Parameter(description = "Порядок сортировки")
    @Schema(implementation = SortOrder.class, example = "desc")
    @RequestParam(required = false, name = "sort_order", defaultValue = "desc")
      SortOrder sortOrder
  ) {

    MeetingSearchDto dto = new MeetingSearchDto();

    dto.setPageNumber(pageNumber);
    dto.setPageSize(pageSize);

    dto.setSortOrder(sortOrder);
    dto.setSortType(sortType);

    dto.setSearchString(searchString);
    dto.setMeetingStatus(meetingStatus);
    dto.setUserId(userId);
    dto.setOrganizerId(organizerId);

    dto.setCategories(categoryMapper.toEntities(categories));
    dto.setGames(gameMapper.toEntities(games));

    dto.setRatingStateStart(ratingStateStart);
    dto.setRatingStateEnd(ratingStateEnd);
    dto.setAddress(address);
    dto.setPeriodStart(periodStart);
    dto.setPeriodEnd(periodEnd);
    dto.setMembersCountStart(membersCountStart);
    dto.setMembersCountEnd(membersCountEnd);
    dto.setMaxMembersCountStart(maxMembersCountStart);
    dto.setMaxMembersCountEnd(maxMembersCountEnd);

    return meetingService.getMeetings(dto);
  }

  @Operation(
    summary = "Получение встречи по id",
    security = {@SecurityRequirement(name = "bearer-key")}
  )
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Найденная встреча по id",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = MeetingDto.class))
      }),
    @ApiResponse(
      responseCode = "400",
      description = "Несуществующй id встречи",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class))
      })
  })
  @GetMapping(value = "/{meeting_id}")
  MeetingDto getMeetingById(
    @PathVariable(name = "meeting_id")
    @Parameter(description = "id встречи")
      UUID meetingId
  ) {
    return meetingService.getMeetingById(meetingId);
  }

  @Operation(
    summary = "Создание встречи",
    security = {@SecurityRequirement(name = "bearer-key")}
  )
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Встреча успешно создана",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = MeetingDto.class))
      }),
    @ApiResponse(
      responseCode = "400",
      description = "Ошибки валидации полученных данных",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class))
      })
  })
  @PostMapping
  MeetingDto create(@RequestBody MeetingCreateDto meetingDto) {
    return meetingService.create(meetingDto);
  }


  @Operation(
    summary = "Обновление данных встречи",
    security = {@SecurityRequirement(name = "bearer-key")}
  )
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Встреча успешно обновлена",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = MeetingDto.class))
      }),
    @ApiResponse(
      responseCode = "400",
      description = "Ошибки валидации полученных данных",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class))
      })
  })
  @PutMapping
  MeetingDto update(@RequestBody MeetingUpdateDto meetingDto) {
    return meetingService.update(meetingDto);
  }

  @Operation(
    summary = "Удаление встречи по id",
    security = {@SecurityRequirement(name = "bearer-key")}
  )
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Встреча успешно удалена"
    ),
    @ApiResponse(
      responseCode = "400",
      description = "Несуществующй id встречи",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class))
      })
  })
  @DeleteMapping("/{meeting_id}")
  void delete(
    @Parameter(description = "id встречи")
    @PathVariable(name = "meeting_id") UUID meetingId
  ) {
    meetingService.delete(meetingId);
  }
}
