package com.example.tabletool.controller.api;

import com.example.tabletool.service.logic.ProductService;
import com.example.tabletool.service.model.ProductDto;
import com.example.tabletool.service.model.TrackedProductDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
@AllArgsConstructor
@Tag(name = "products")
public class ProductController {

  private final ProductService productService;

  @Operation(
    summary = "Получение списка товаров, найденных по строке поиска"
  )
  @GetMapping
  List<ProductDto> getProducts(
    @Parameter(description = "Строка поиска")
    @RequestParam(name = "search")
    String search
  ) {
    return productService.getProducts(search);
  }

  @Operation(
    summary = "Добавление товара в список отслеживаемых",
    security = {@SecurityRequirement(name = "bearer-key")}
  )
  @PostMapping("/tracked")
  ProductDto addToTrackedList(
    @RequestBody
    TrackedProductDto productDto
  ) {
    return productService.addToTrackedList(productDto);
  }

  @Operation(
    summary = "Получение списка отслеживаемых товаров пользователя",
    security = {@SecurityRequirement(name = "bearer-key")}
  )
  @GetMapping("/tracked")
  List<ProductDto> getTrackedList() {
    return productService.getTrackedList();
  }
}
