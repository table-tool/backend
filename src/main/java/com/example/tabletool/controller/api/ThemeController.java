package com.example.tabletool.controller.api;

import com.example.tabletool.persistence.entity.ThemeType;
import com.example.tabletool.service.exception.ExceptionDto;
import com.example.tabletool.service.logic.ThemeService;
import com.example.tabletool.service.model.ThemeDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/themes")
@AllArgsConstructor
@Tag(name = "themes")
public class ThemeController {

  private final ThemeService themeService;

  @Operation(summary = "Получение всех тем")
  @ApiResponse(
    responseCode = "200",
    description = "Найдены все темы",
    content = {
      @Content(
        mediaType = "application/json",
        array = @ArraySchema(schema = @Schema(implementation = ThemeType.class)))
    })
  @GetMapping
  List<ThemeDto> getAllThemes() {
    return themeService.getAllThemes();
  }

  @Operation(summary = "Получение темы по заданному типу")
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Найдены все темы",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ThemeType.class))
      }),
    @ApiResponse(
      responseCode = "400",
      description = "Неверный тип темы",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class))
      })
  })
  @GetMapping("/{theme_type}")
  ThemeDto getThemeByType(
    @PathVariable(name = "theme_type")
    @Schema(implementation = ThemeType.class)
    @Parameter(description = "Тип темы")
      ThemeType themeType
  ) {
    return themeService.getThemeByType(themeType);
  }
}
