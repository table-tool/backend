package com.example.tabletool.controller.api;

import com.example.tabletool.persistence.entity.CategoryEnum;
import com.example.tabletool.service.logic.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/categories")
@AllArgsConstructor
@Tag(name = "categories")
public class CategoryController {

  private final CategoryService categoryService;

  @Operation(summary = "Получение списка категорий игр")
  @ApiResponse(
    responseCode = "200",
    description = "Найдены все категории",
    content = {
      @Content(
        mediaType = "application/json",
        array = @ArraySchema(schema = @Schema(implementation = CategoryEnum.class)))
    })
  @GetMapping
  List<CategoryEnum> getAllCategories() {
    return categoryService.getAllCategories();
  }
}
