package com.example.tabletool.controller.api;

import com.example.tabletool.service.exception.ExceptionDto;
import com.example.tabletool.service.logic.MeetingService;
import com.example.tabletool.service.logic.UserService;
import com.example.tabletool.service.model.UserCreateDto;
import com.example.tabletool.service.model.UserDto;
import com.example.tabletool.service.model.UserUpdateDto;
import com.example.tabletool.service.model.ValidDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
@Tag(name = "users")
public class UserController {

  private final MeetingService meetingService;

  private final UserService userService;

  @Operation(
    summary = "Добавление пользователя к встрече",
    security = {@SecurityRequirement(name = "bearer-key")}
  )
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Успешное добавление пользователя к встречи"
    ),
    @ApiResponse(
      responseCode = "400",
      description = "Несуществующий id встречи, пользователь уже находится в данной встрече",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class)
        )}
    )}
  )
  @PostMapping("/{user_id}/meeting/{meeting_id}")
  void addUserToMeeting(
    @Parameter(description = "id встречи")
    @PathVariable(name = "meeting_id")
      UUID meetingId,
    @Parameter(description = "id пользователя")
    @PathVariable(name = "user_id")
      String thisUserId
  ) {
    meetingService.addUserToMeeting(meetingId, thisUserId);
  }


  @Operation(
    summary = "Удаление пользователя из встречи",
    security = {@SecurityRequirement(name = "bearer-key")}
  )
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Успешное удаление пользователя из состава встречи"
    ),
    @ApiResponse(
      responseCode = "400",
      description = "Несуществующие id, пользователь не находится в данной встрече",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class)
        )}
    )}
  )
  @DeleteMapping("/{user_id}/meeting/{meeting_id}")
  void deleteUserFromMeeting(
    @Parameter(description = "id встречи")
    @PathVariable(name = "meeting_id")
      UUID meetingId,
    @Parameter(description = "id пользователя")
    @PathVariable(name = "user_id")
      String thisUserId
  ) {
    meetingService.deleteUserFromMeeting(meetingId, thisUserId);
  }

  @Operation(
    summary = "Проверка имени пользователя на уникальность"
  )
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Результат проверки"
    ),
    @ApiResponse(
      responseCode = "400",
      description = "Пустое имя пользователя",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class)
        )}
    )}
  )
  @GetMapping("/username")
  ValidDto checkValid(
    @Parameter(description = "Имя пользователя во внешнем ресурсе")
    @RequestParam
      String username
  ) {
    ValidDto validDto = new ValidDto();
    validDto.setValid(userService.checkValid(username));

    return validDto;
  }

  @Operation(
    summary = "Создание пользователя"
  )
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Успешное создание пользователя"
    ),
    @ApiResponse(
      responseCode = "400",
      description = "Имя или id пользователя уже существуют",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class)
        )}
    )}
  )
  @PostMapping
  UserDto create(@RequestBody UserCreateDto userCreateDto) {
    return userService.create(userCreateDto);
  }

  @Operation(
    summary = "Обновление данных пользователя",
    security = {@SecurityRequirement(name = "bearer-key")}
  )
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Успешное обновление данных пользователя"
    ),
    @ApiResponse(
      responseCode = "400",
      description = "Имя пользователя уже существует или id пользователя не существуют",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class)
        )}
    )}
  )
  @PutMapping
  UserDto update(@RequestBody UserUpdateDto userUpdateDto) {
    return userService.update(userUpdateDto);
  }

  @Operation(
    summary = "Получить данные о пользователе по id",
    security = {@SecurityRequirement(name = "bearer-key")}
  )
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Данные о пользователе"
    ),
    @ApiResponse(
      responseCode = "400",
      description = "id пользователя не существует",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class)
        )}
    )}
  )
  @GetMapping("/{user_id}")
  UserDto getById(@PathVariable(name = "user_id") String userId) {
    return userService.getById(userId);
  }
}
