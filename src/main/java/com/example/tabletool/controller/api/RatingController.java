package com.example.tabletool.controller.api;

import com.example.tabletool.persistence.entity.RatingMark;
import com.example.tabletool.security.SecurityService;
import com.example.tabletool.service.exception.ApiRequestException;
import com.example.tabletool.service.exception.ExceptionDto;
import com.example.tabletool.service.logic.RatingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/ratings")
@AllArgsConstructor
@Tag(name = "ratings")
public class RatingController {

  private final RatingService ratingService;

  private final SecurityService securityService;

  @Operation(
    summary = "Выставление оценки участнику встречи",
    security = {@SecurityRequirement(name = "bearer-key")}
  )
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Успешная оценивнаие участника встречи"
    ),
    @ApiResponse(
      responseCode = "400",
      description = "Пользователь оценивает сам себя, несуществующие id или происходит оценивание по id пользователя, не входящего в заданную встречу",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class)
        )}
    )}
  )
  @PostMapping("/meeting/{meeting_id}/player/{player_id}/rate/{rate}")
  void ratePlayerInMeeting(
    @Parameter(description = "id встречи")
    @PathVariable(name = "meeting_id")
      UUID meetingId,
    @Parameter(description = "id оцениваемого")
    @PathVariable(name = "player_id")
      String playerId,
    @Parameter(description = "id оцениваемого")
    @Schema(implementation = RatingMark.class)
    @PathVariable(name = "rate")
      RatingMark rate
  ) {
    if (playerId.equals(securityService.getUserId())) {
      throw new ApiRequestException("Игрок не может оценить самого себя");
    }

    ratingService.ratePlayerInMeeting(meetingId, playerId, rate, securityService.getUserId());
  }

  @Operation(
    summary = "Удаление оценки участника встречи",
    security = {@SecurityRequirement(name = "bearer-key")}
  )
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Успешная удаление оценки участника встречи"
    ),
    @ApiResponse(
      responseCode = "400",
      description = "Несуществующие id",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class)
        )}
    )}
  )
  @DeleteMapping("/meeting/{meeting_id}/player/{player_id}")
  void unratePlayerInMeeting(
    @Parameter(description = "id встречи")
    @PathVariable(name = "meeting_id")
      UUID meetingId,
    @Parameter(description = "id оцениваемого")
    @PathVariable(name = "player_id")
      String playerId
  ) {
    if (!playerId.equals(securityService.getUserId())) {
      ratingService.unratePlayerInMeeting(meetingId, playerId, securityService.getUserId());
    }
  }
}
