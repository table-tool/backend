package com.example.tabletool.controller.api;

import com.example.tabletool.persistence.entity.CategoryEnum;
import com.example.tabletool.service.exception.ExceptionDto;
import com.example.tabletool.service.logic.GameService;
import com.example.tabletool.service.model.GameDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/games")
@AllArgsConstructor
@Tag(name = "games")
public class GameController {

  private final GameService gameService;

  @Operation(summary = "Получение названий всех игр")
  @ApiResponse(
    responseCode = "200",
    description = "Найдены все игры",
    content = {
      @Content(
        mediaType = "application/json",
        array = @ArraySchema(schema = @Schema(implementation = GameDto.class)))
    })
  @GetMapping
  List<GameDto> getAllGames() {
    return gameService.getAllGames();
  }

  @Operation(summary = "Получение названий игр по заданной категории")
  @ApiResponses(value = {
    @ApiResponse(
      responseCode = "200",
      description = "Найдены игры по заданной категории",
      content = {
        @Content(
          mediaType = "application/json",
          array = @ArraySchema(schema = @Schema(implementation = GameDto.class)))
      }),
    @ApiResponse(
      responseCode = "400",
      description = "Неверное имя категории",
      content = {
        @Content(
          mediaType = "application/json",
          schema = @Schema(implementation = ExceptionDto.class))
      })
  })
  @GetMapping("/category/{category_name}")
  List<GameDto> getAllGamesByCategory(
    @PathVariable(name = "category_name")
    @Schema(implementation = CategoryEnum.class)
    @Parameter(description = "Категория игры")
    CategoryEnum category
  ) {
    return gameService.getAllGamesByCategory(category);
  }
}
