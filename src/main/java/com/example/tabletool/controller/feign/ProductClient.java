package com.example.tabletool.controller.feign;

import com.example.tabletool.service.model.PriceDto;
import com.example.tabletool.service.model.ProductDtoList;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
  name = "product-client",
  url = "https://pp.johns-last-delight.keenetic.pro"
)
public interface ProductClient {

  @GetMapping
  ProductDtoList getProducts(
    @RequestParam(required = false, name = "search") String search,
    @RequestParam(required = false, name = "c") String classname,
    @RequestParam(required = false, name = "n") String name,
    @RequestParam(required = false, name = "p") String price
  );

  @GetMapping
  PriceDto getPrice(
    @RequestParam(required = false, name = "link") String link,
    @RequestParam(required = false, name = "l") String classLink
  );
}
