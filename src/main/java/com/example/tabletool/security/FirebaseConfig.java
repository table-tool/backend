package com.example.tabletool.security;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;

@Configuration
@AllArgsConstructor
public class FirebaseConfig {
  private final ResourceLoader resourceLoader;
  @Primary
  @Bean
  public FirebaseApp getFirebaseApp() throws IOException {
    Resource resource = resourceLoader.getResource("classpath:serviceAccountKey.json");

    FirebaseOptions options = FirebaseOptions.builder()
      .setCredentials(GoogleCredentials.fromStream(resource.getInputStream()))
      .setDatabaseUrl("https://table-tool-default-rtdb.firebaseio.com")
      .build();

    if (FirebaseApp.getApps().isEmpty()) {
      FirebaseApp.initializeApp(options);
    }

    return FirebaseApp.getInstance();
  }

  @Bean
  public FirebaseAuth getAuth() throws IOException {
    return FirebaseAuth.getInstance(getFirebaseApp());
  }
}
