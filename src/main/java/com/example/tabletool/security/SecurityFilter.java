package com.example.tabletool.security;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
@Slf4j
public class SecurityFilter extends OncePerRequestFilter {

  private final FirebaseAuth firebaseAuth;

  @Override
  protected void doFilterInternal(
    @NonNull HttpServletRequest request,
    @NonNull HttpServletResponse response,
    FilterChain filterChain
  ) throws ServletException, IOException {
    authorize(request);
    filterChain.doFilter(request, response);
  }

  private void authorize(HttpServletRequest request) {
    String authorizationHeader = request.getHeader("Authorization");
    if (authorizationHeader != null) {
      if (authorizationHeader.startsWith("Bearer")) {
        authorizationHeader = authorizationHeader.substring(7);
      }

      try {
        FirebaseToken decodedToken = firebaseAuth.verifyIdToken(authorizationHeader, true);

        List<GrantedAuthority> authorities = new ArrayList<>();
        decodedToken.getClaims().forEach((k, v) -> authorities.add(new SimpleGrantedAuthority(k)));

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken("user", decodedToken.getUid(), authorities);
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        SecurityContextHolder.getContext().setAuthentication(authentication);

      } catch (FirebaseAuthException | IllegalArgumentException | NullPointerException ignore) {
      }
    }
  }
}
