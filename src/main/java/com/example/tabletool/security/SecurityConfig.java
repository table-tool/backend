package com.example.tabletool.security;

import com.example.tabletool.service.exception.ExceptionDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.time.LocalDateTime;
import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
@AllArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  private final SecurityFilter tokenAuthenticationFilter;
  private final ObjectMapper objectMapper;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .cors().disable()
      .csrf().disable()
      .formLogin().disable()
      .httpBasic().disable()
      .exceptionHandling()
      .authenticationEntryPoint(restAuthenticationEntryPoint())
      .and()
      .authorizeRequests()
      .antMatchers(
        HttpMethod.POST,
        List.of(
          "/users"
        ).toArray(new String[0])).permitAll()
      .antMatchers(
        HttpMethod.GET,
        List.of(
          "/swagger-ui.html",
          "/swagger-ui/*",
          "/categories",
          "/users/username",
          "/themes",
          "/products",
          "/themes/{theme_type}",
          "/games",
          "/games/category/{category_name}",
          "/v3/api-docs/swagger-config",
          "/v3/api-docs"
        ).toArray(new String[0])).permitAll()
      .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
      .anyRequest().authenticated()
      .and()
      .addFilterBefore(tokenAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
      .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }

  @Bean
  public AuthenticationEntryPoint restAuthenticationEntryPoint() {
    return (httpServletRequest, httpServletResponse, e) -> {

      httpServletResponse.setContentType("application/json;charset=UTF-8");
      httpServletResponse.setStatus(401);

      ExceptionDto exceptionDto = new ExceptionDto(
        List.of("Unauthorized access of protected resource, invalid credentials"),
        HttpStatus.UNAUTHORIZED,
        LocalDateTime.now()
      );

      httpServletResponse.getWriter().write(objectMapper.writeValueAsString(exceptionDto));
    };
  }
}
