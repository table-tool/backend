package com.example.tabletool.security;

import com.example.tabletool.service.exception.ApiRequestException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurityService {

  public String getUserId() {
    SecurityContext securityContext = SecurityContextHolder.getContext();
    Object principal = securityContext.getAuthentication().getCredentials();
    if (principal instanceof String uid) {
      return uid;
    } else {
      throw new ApiRequestException("Unauthorized access of protected resource, invalid credentials");
    }
  }

}
