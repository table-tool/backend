package com.example.tabletool.persistence.specification.meeting;

import com.example.tabletool.persistence.entity.Game;
import com.example.tabletool.persistence.entity.Meeting;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.NonNull;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class MeetingSearchSpecification implements Specification<Meeting> {

  private String searchString;

  private List<Game> gamesFromString;

  @Override
  public Predicate toPredicate(@NonNull Root<Meeting> meeting, @NonNull CriteriaQuery<?> query, @NonNull CriteriaBuilder builder) {

    if (searchString != null) {

      String pattern = "%" + searchString.toUpperCase() + "%";

      Predicate description = builder.like(builder.upper(meeting.get("description")), pattern);

      Expression<List<Game>> games = meeting.get("games");

      List<Predicate> predicates = new ArrayList<>();
      for (Game game : gamesFromString) {
        predicates.add(builder.isMember(game, games));
      }

      Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));

      return builder.or(description, gamesFromString.isEmpty() ? builder.or() : predicate);
    }

    return builder.and();
  }

  @Override
  @NonNull
  public Specification<Meeting> and(Specification<Meeting> other) {
    return Specification.super.and(other);
  }

  @Override
  @NonNull
  public Specification<Meeting> or(Specification<Meeting> other) {
    return Specification.super.or(other);
  }
}
