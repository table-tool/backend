package com.example.tabletool.persistence.specification.meeting;

import com.example.tabletool.persistence.entity.*;
import com.example.tabletool.service.model.MeetingSearchDto;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.NonNull;

import javax.persistence.criteria.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.example.tabletool.persistence.entity.MeetingStatus.FUTURE;

@AllArgsConstructor
public class MeetingFilterSpecification implements Specification<Meeting> {

  private MeetingSearchDto dto;

  @Override
  public Predicate toPredicate(@NonNull Root<Meeting> meeting, @NonNull CriteriaQuery<?> query, @NonNull CriteriaBuilder builder) {

    List<Predicate> predicates = new ArrayList<>();

    User user = dto.getUser();
    if (user != null) {
      Expression<List<User>> members = meeting.get("members");

      predicates.add(builder.isMember(user, members));
    }

    String organizerId = dto.getOrganizerId();
    if (organizerId != null) {
      Path<User> organizer = meeting.get("organizer");
      Path<String> userId = organizer.get("id");

      predicates.add(builder.equal(userId, organizerId));
    }

    MeetingStatus meetingStatus = dto.getMeetingStatus();
    if (meetingStatus != null) {
      Expression<LocalDateTime> currentDate = builder.literal(LocalDateTime.now());

      Predicate predicate = meetingStatus.equals(FUTURE) ?
        builder.greaterThan(meeting.get("startDate"), currentDate) :
        builder.lessThanOrEqualTo(meeting.get("startDate"), currentDate);

      predicates.add(predicate);
    }

    String address = dto.getAddress();
    if (address != null) {
      String pattern = "%" + address.toUpperCase() + "%";

      Predicate predicate = builder.like(builder.upper(meeting.get("address")), pattern);

      predicates.add(predicate);
    }

    List<Game> games = dto.getGames();
    if (games != null) {
      Expression<List<Game>> gamesFromDb = meeting.get("games");

      for (Game game : games) {
        predicates.add(builder.isMember(game, gamesFromDb));
      }
    }

    List<Category> categories = dto.getCategories();
    if (categories != null) {
      Expression<List<Category>> categoriesFromDb = meeting.get("categories");

      for (Category category : categories) {
        predicates.add(builder.isMember(category, categoriesFromDb));
      }
    }

    RatingState ratingStateStart = dto.getRatingStateStart();
    RatingState ratingStateEnd = dto.getRatingStateEnd();
    if (ratingStateStart != null && ratingStateEnd != null) {
      Path<User> organizer = meeting.get("organizer");
      Path<Integer> pros = organizer.get("pros");
      Path<Integer> cons = organizer.get("cons");

      Expression<Integer> rate = builder.quot(builder.prod(pros, 100), builder.sum(pros, cons)).as(Integer.class);

      predicates.add(builder.between(rate, ratingStateStart.getStart(), ratingStateEnd.getEnd()));
    }

    Integer periodStart = dto.getPeriodStart();
    Integer periodEnd = dto.getPeriodEnd();
    if (periodStart != null && periodEnd != null) {
      Path<Integer> periodFromDb = meeting.get("period");

      predicates.add(builder.between(periodFromDb, periodStart, periodEnd));
    }

    Integer playersCountStart = dto.getMembersCountStart();
    Integer playersCountEnd = dto.getMembersCountEnd();
    if (playersCountStart != null && playersCountEnd != null) {
      Expression<Integer> membersSize = builder.size(meeting.get("members"));

      predicates.add(builder.between(membersSize, playersCountStart, playersCountEnd));
    }

    Integer maxMembersCountStart = dto.getMaxMembersCountStart();
    Integer maxMembersCountEnd = dto.getMaxMembersCountEnd();
    if (maxMembersCountStart != null && maxMembersCountEnd != null) {
      Expression<Integer> membersMaxSize = meeting.get("maxMembersCount");

      predicates.add(builder.between(membersMaxSize, maxMembersCountStart, maxMembersCountEnd));
    }

    return builder.and(predicates.toArray(new Predicate[0]));
  }

  @Override
  @NonNull
  public Specification<Meeting> and(Specification<Meeting> other) {
    return Specification.super.and(other);
  }

  @Override
  @NonNull
  public Specification<Meeting> or(Specification<Meeting> other) {
    return Specification.super.or(other);
  }
}
