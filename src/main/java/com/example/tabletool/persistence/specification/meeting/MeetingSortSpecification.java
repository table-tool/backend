package com.example.tabletool.persistence.specification.meeting;

import com.example.tabletool.persistence.entity.Meeting;
import com.example.tabletool.persistence.entity.SortOrder;
import com.example.tabletool.persistence.entity.SortType;
import com.example.tabletool.persistence.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.NonNull;

import javax.persistence.criteria.*;

import static com.example.tabletool.persistence.entity.SortOrder.ASC;

@AllArgsConstructor
public class MeetingSortSpecification implements Specification<Meeting> {

  SortType sortType;
  SortOrder sortOrder;

  @Override
  public Predicate toPredicate(@NonNull Root<Meeting> meeting, @NonNull CriteriaQuery<?> query, @NonNull CriteriaBuilder builder) {

    if (sortType != null && sortOrder != null) {

      var order = switch (sortType) {
        case START_DATE -> meeting.get("startDate");
        case PERIOD -> meeting.get("period");
        case MAX_PLAYERS_COUNT -> meeting.get("maxPlayersCount");
        case MEMBERS_COUNT -> builder.size(meeting.get("members"));
        case JOIN_AVAILABILITY -> {
          Expression<Integer> membersSize = builder.size(meeting.get("members"));
          Path<Integer> maxPlayersCount = meeting.get("maxPlayersCount");

          Expression<Integer> diff = builder.diff(maxPlayersCount, membersSize);

          yield builder.quot(builder.prod(diff, 100), maxPlayersCount);
        }
        case ORGANIZER_RATING -> {
          Path<User> organizer = meeting.get("organizer");
          Path<Integer> pros = organizer.get("pros");
          Path<Integer> cons = organizer.get("cons");

          yield builder.quot(builder.prod(pros, 100), builder.sum(pros, cons));
        }
      };

      query.orderBy(sortOrder.equals(ASC) ? builder.asc(order) : builder.desc(order));
    }

    return builder.and();
  }

  @Override
  @NonNull
  public Specification<Meeting> and(Specification<Meeting> other) {
    return Specification.super.and(other);
  }

  @Override
  @NonNull
  public Specification<Meeting> or(Specification<Meeting> other) {
    return Specification.super.or(other);
  }
}
