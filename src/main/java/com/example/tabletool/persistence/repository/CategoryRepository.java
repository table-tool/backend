package com.example.tabletool.persistence.repository;

import com.example.tabletool.persistence.entity.Category;
import com.example.tabletool.persistence.entity.CategoryEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface CategoryRepository extends JpaRepository<Category, UUID> {

  Optional<Category> findByName(CategoryEnum name);
}
