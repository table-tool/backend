package com.example.tabletool.persistence.repository;

import com.example.tabletool.persistence.entity.Theme;
import com.example.tabletool.persistence.entity.ThemeType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ThemeRepository extends JpaRepository<Theme, UUID> {
  Theme findByThemeType(ThemeType themeType);

}
