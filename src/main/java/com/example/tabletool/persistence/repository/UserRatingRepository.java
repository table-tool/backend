package com.example.tabletool.persistence.repository;

import com.example.tabletool.persistence.entity.UserRating;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserRatingRepository extends JpaRepository<UserRating, UUID> {


}
