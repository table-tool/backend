package com.example.tabletool.persistence.repository;

import com.example.tabletool.persistence.entity.ProductTag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ProductTagRepository extends JpaRepository<ProductTag, UUID>  {
}
