package com.example.tabletool.persistence.repository;

import com.example.tabletool.persistence.entity.CategoryEnum;
import com.example.tabletool.persistence.entity.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GameRepository extends JpaRepository<Game, UUID> {

  Optional<Game> findByName(String name);

  List<Game> findAllByCategories_nameEquals(CategoryEnum name);

  @Query("select g from Game g where upper(g.name) LIKE %?1%")
  List<Game> searchByName(String name);
}
