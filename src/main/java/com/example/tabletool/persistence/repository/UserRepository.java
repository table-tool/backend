package com.example.tabletool.persistence.repository;

import com.example.tabletool.persistence.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {

  boolean existsByUsername(String username);
}
