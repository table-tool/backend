package com.example.tabletool.persistence.converter;

import com.example.tabletool.persistence.entity.CategoryEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class CategoryEnumConverter implements AttributeConverter<CategoryEnum, String> {

  @Override
  public String convertToDatabaseColumn(CategoryEnum attribute) {
    return attribute == null ? null : attribute.getName();
  }

  @Override
  public CategoryEnum convertToEntityAttribute(String dbData) {
    return CategoryEnum.fromName(dbData).orElse(null);
  }
}
