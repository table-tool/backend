package com.example.tabletool.persistence.converter;

import com.example.tabletool.persistence.entity.RatingMark;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RatingMarkConverter implements AttributeConverter<RatingMark, Integer> {

  @Override
  public Integer convertToDatabaseColumn(RatingMark attribute) {
    return attribute == null ? null : attribute.getId();
  }

  @Override
  public RatingMark convertToEntityAttribute(Integer dbData) {
    return RatingMark.fromId(dbData).orElse(null);
  }
}
