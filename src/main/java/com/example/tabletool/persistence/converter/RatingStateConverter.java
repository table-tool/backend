package com.example.tabletool.persistence.converter;

import com.example.tabletool.persistence.entity.RatingState;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RatingStateConverter implements AttributeConverter<RatingState, Integer> {

  @Override
  public Integer convertToDatabaseColumn(RatingState attribute) {
    return attribute == null ? null : attribute.getId();
  }

  @Override
  public RatingState convertToEntityAttribute(Integer dbData) {
    return RatingState.fromId(dbData).orElse(null);
  }
}
