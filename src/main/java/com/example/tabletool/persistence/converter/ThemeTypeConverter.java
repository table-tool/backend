package com.example.tabletool.persistence.converter;

import com.example.tabletool.persistence.entity.ThemeType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


@Converter(autoApply = true)
public class ThemeTypeConverter implements AttributeConverter<ThemeType, String> {

  @Override
  public String convertToDatabaseColumn(ThemeType attribute) {
    return attribute == null ? null : attribute.getName();
  }

  @Override
  public ThemeType convertToEntityAttribute(String dbData) {
    return ThemeType.fromName(dbData).orElse(null);
  }
}
