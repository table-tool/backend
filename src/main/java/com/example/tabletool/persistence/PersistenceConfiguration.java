package com.example.tabletool.persistence;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EntityScan(basePackages = {
  "com.example.tabletool.persistence.entity",
  "com.example.tabletool.persistence.converter"
})
@EnableJpaRepositories(basePackages = "com.example.tabletool.persistence.repository")
@EnableTransactionManagement
public class PersistenceConfiguration {
}
