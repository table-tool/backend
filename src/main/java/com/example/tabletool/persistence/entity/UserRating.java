package com.example.tabletool.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "users_ratings_meetings", schema = "table_tool")
public class UserRating {

  @Id
  @GeneratedValue
  @Column(name = "users_ratings_meetings_id")
  private UUID id;

  @Column(name = "evaluator_id")
  private String evaluatorId;

  @Column(name = "user_id")
  private String userId;

  @Column(name = "rating_mark")
  private RatingMark ratingMark;

  @Column(name = "meeting_id")
  private UUID meetingId;
}
