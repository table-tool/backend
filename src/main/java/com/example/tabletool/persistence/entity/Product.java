package com.example.tabletool.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "products", schema = "table_tool")
public class Product {

  @Id
  @GeneratedValue
  @Column(name = "product_id")
  private UUID id;

  @Column(name = "product_name")
  private String name;
  @Column(name = "price")
  private BigDecimal price;
  @Column(name = "link")
  private String link;
  @Column(name = "photo_link")
  private String photoLink;

  @Column(name = "user_id")
  private String ownerId;
}
