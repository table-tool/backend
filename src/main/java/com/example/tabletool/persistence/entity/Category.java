package com.example.tabletool.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "categories", schema = "table_tool")
public class Category {

  @Id
  @GeneratedValue
  @Column(name = "category_id")
  private UUID id;

  private CategoryEnum name;
}
