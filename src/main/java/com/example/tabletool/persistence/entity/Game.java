package com.example.tabletool.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "games", schema = "table_tool")
public class Game {

  @Id
  @GeneratedValue
  @Column(name = "game_id")
  private UUID id;

  private String name;

  private String photo;

  @ManyToMany
  @JoinTable(
    name = "games_categories",
    schema = "table_tool",
    joinColumns = {@JoinColumn(name = "game_id")},
    inverseJoinColumns = {@JoinColumn(name = "category_id")}
  )
  private List<Category> categories;
}
