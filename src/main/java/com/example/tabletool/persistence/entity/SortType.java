package com.example.tabletool.persistence.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;
import java.util.Optional;

@Schema(description = "Тип сортировки")
public enum SortType {

  @Schema(description = "Начальная дата")
  START_DATE("start_date"),
  @Schema(description = "Продолжительность")
  PERIOD("period"),
  @Schema(description = "Максимальное число участников")
  MAX_PLAYERS_COUNT("max_players_count"),
  @Schema(description = "Рейтинг организатора")
  ORGANIZER_RATING("organizer_rating"),
  @Schema(description = "Доступность вступления")
  JOIN_AVAILABILITY("join_availability"),
  @Schema(description = "Число участников")
  MEMBERS_COUNT("members_count");

  private final String name;

  SortType(String name) {
    this.name = name;
  }

  public static Optional<SortType> fromName(String name) {
    if (name == null) {
      return Optional.empty();
    }

    for (var value : SortType.values()) {
      if (Objects.equals(value.name, name)) {
        return Optional.of(value);
      }
    }

    return Optional.empty();
  }

  @JsonValue
  public String getName() {
    return name;
  }
}
