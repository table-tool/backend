package com.example.tabletool.persistence.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;
import java.util.Optional;

@Schema(description = "Порядок сортировки")
public enum SortOrder {

  @Schema(name = "По возрастанию")
  ASC("asc"),
  @Schema(name = "По убыванию")
  DESC("desc");

  private final String name;

  SortOrder(String name) {
    this.name = name;
  }

  public static Optional<SortOrder> fromName(String name) {
    if (name == null) {
      return Optional.empty();
    }

    for (var value : SortOrder.values()) {
      if (Objects.equals(value.name, name)) {
        return Optional.of(value);
      }
    }

    return Optional.empty();
  }

  @JsonValue
  public String getName() {
    return name;
  }
}
