package com.example.tabletool.persistence.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;
import java.util.Optional;

@Schema(description = "Рейтинг пользователя")
public enum RatingState {

  NEGATIVE(0, 0, 20),
  MOSTLY_NEGATIVE(1, 21, 40),
  NEUTRAL(2, 41, 60),
  MOSTLY_POSITIVE(3, 61, 80),
  POSITIVE(4, 81, 100);

  private final int id;
  private final int start;
  private final int end;

  RatingState(int id, int start, int end) {
    this.id = id;
    this.start = start;
    this.end = end;
  }

  public static Optional<RatingState> fromId(Integer id) {
    if (id == null) {
      return Optional.empty();
    }

    for (var value : RatingState.values()) {
      if (value.id == id) {
        return Optional.of(value);
      }
    }

    return Optional.empty();
  }

  public static Optional<RatingState> fromStringId(String stringId) {
    if (stringId == null) {
      return Optional.empty();
    }

    for (var value : RatingState.values()) {
      try {
        if (Objects.equals(value.id, Integer.parseInt(stringId))) {
          return Optional.of(value);
        }
      } catch (NumberFormatException e) {
        return Optional.empty();
      }
    }

    return Optional.empty();
  }

  @JsonValue
  public int getId() {
    return id;
  }

  public int getStart() {
    return start;
  }

  public int getEnd() {
    return end;
  }
}
