package com.example.tabletool.persistence.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;
import java.util.Optional;

@Schema(description = "Оценка пользователя")
public enum RatingMark {

  LIKE(1, "like"),
  DISLIKE(-1, "dislike"),
  ;

  private final int id;

  private final String name;

  RatingMark(int id, String name) {
    this.id = id;
    this.name = name;
  }

  public static Optional<RatingMark> fromId(Integer id) {
    if (id == null) {
      return Optional.empty();
    }

    for (var value : RatingMark.values()) {
      if (id == value.id) {
        return Optional.of(value);
      }
    }

    return Optional.empty();
  }


  public static Optional<RatingMark> fromName(String name) {
    if (name == null) {
      return Optional.empty();
    }

    for (var value : RatingMark.values()) {
      if (Objects.equals(value.name, name)) {
        return Optional.of(value);
      }
    }

    return Optional.empty();
  }

  public int getId() {
    return id;
  }

  @JsonValue
  public String getName() {
    return name;
  }
}
