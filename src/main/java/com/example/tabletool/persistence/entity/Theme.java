package com.example.tabletool.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "themes", schema = "table_tool")
public class Theme {

  @Id
  @Column(name = "theme_id")
  private UUID themeId;

  @Column(name = "theme_type")
  private ThemeType themeType;

  @Column(name = "background")
  private String background;
  @Column(name = "item_background")
  private String itemBackground;
  @Column(name = "button_primary")
  private String buttonPrimary;
  @Column(name = "separator_color")
  private String separatorColor;
  @Column(name = "white_element")
  private String whiteElement;
  @Column(name = "black_element")
  private String blackElement;
  @Column(name = "color_secondary")
  private String colorSecondary;
  @Column(name = "text_hint")
  private String textHint;
  @Column(name = "settings_header")
  private String settingsHeader;
}
