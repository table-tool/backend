package com.example.tabletool.persistence.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;
import java.util.Optional;

@Schema(description = "Тип темы")
public enum ThemeType {

  @Schema(description = "Белая")
  WHITE("white"),
  @Schema(description = "Тёмная")
  DARK("dark");

  private final String name;

  ThemeType(String name) {
    this.name = name;
  }

  public static Optional<ThemeType> fromName(String name) {
    if (name == null) {
      return Optional.empty();
    }

    for (var value : ThemeType.values()) {
      if (Objects.equals(value.name, name)) {
        return Optional.of(value);
      }
    }

    return Optional.empty();
  }

  @JsonValue
  public String getName() {
    return name;
  }
}
