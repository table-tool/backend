package com.example.tabletool.persistence.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;
import java.util.Optional;

@Schema(description = "Тип встречи")
public enum MeetingStatus {

  FUTURE("future"),
  PAST("past");

  private final String name;

  MeetingStatus(String name) {
    this.name = name;
  }

  public static Optional<MeetingStatus> fromName(String name) {
    if (name == null) {
      return Optional.empty();
    }

    for (var value : MeetingStatus.values()) {
      if (Objects.equals(value.name, name)) {
        return Optional.of(value);
      }
    }

    return Optional.empty();
  }

  @JsonValue
  public String getName() {
    return name;
  }
}
