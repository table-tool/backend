package com.example.tabletool.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "product_tags", schema = "table_tool")
public class ProductTag {

  @Id
  @Column(name = "product_tag_id")
  private UUID id;

  @Column(name = "class_name")
  private String className;
  @Column(name = "name")
  private String name;
  @Column(name = "price")
  private String price;
  @Column(name = "link")
  private String link;
}
