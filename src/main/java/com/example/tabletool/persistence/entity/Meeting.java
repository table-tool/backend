package com.example.tabletool.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "meetings", schema = "table_tool")
public class Meeting {

  @Id
  @GeneratedValue
  @Column(name = "meeting_id")
  private UUID id;

  @Column(name = "start_date")
  private LocalDateTime startDate;

  private String address;

  private String description;

  @Column(name = "max_players_count", nullable = false, columnDefinition = "integer default 1")
  private int maxPlayersCount;

  private int period;

  @Column(name = "chat_id")
  private String chatId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "organizer_id")
  private User organizer;

  @ManyToMany
  @JoinTable(
    name = "users_meetings",
    schema = "table_tool",
    joinColumns = {@JoinColumn(name = "meeting_id")},
    inverseJoinColumns = {@JoinColumn(name = "user_id")}
  )
  private List<User> members;

  @ManyToMany
  @JoinTable(
    name = "meetings_games",
    schema = "table_tool",
    joinColumns = {@JoinColumn(name = "meeting_id")},
    inverseJoinColumns = {@JoinColumn(name = "game_id")}
  )
  private List<Game> games;

  @ManyToMany
  @JoinTable(
    name = "meetings_categories",
    schema = "table_tool",
    joinColumns = {@JoinColumn(name = "meeting_id")},
    inverseJoinColumns = {@JoinColumn(name = "category_id")}
  )
  private List<Category> categories;

  @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
  @JoinColumn(name = "meeting_id")
  private List<UserRating> usersRating;
}
