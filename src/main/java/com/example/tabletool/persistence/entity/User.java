package com.example.tabletool.persistence.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

import static com.example.tabletool.persistence.entity.RatingState.*;

@Getter
@Setter
@Entity
@Table(name = "users", schema = "table_tool")
public class User {

  @Id
  @Column(name = "user_id")
  private String id;

  @Column(name = "username", unique = true)
  private String username;

  @ColumnDefault("5")
  @Column(name = "pros", nullable = false, columnDefinition = "integer default 5")
  private int pros;

  @ColumnDefault("5")
  @Column(name = "cons", nullable = false, columnDefinition = "integer default 5")
  private int cons;

  @Transient
  public void addRating(RatingMark ratingMark) {
    switch (ratingMark) {
      case LIKE -> pros++;
      case DISLIKE -> cons++;
    }
  }

  @Transient
  public void reduceRating(RatingMark ratingMark) {
    switch (ratingMark) {
      case LIKE -> pros--;
      case DISLIKE -> cons--;
    }
  }

  @Transient
  public RatingState getRatingState() {

    if (pros == 0 && cons == 0) return NEUTRAL;

    int rating = 100 * pros / (pros + cons);

    if (rating >= NEGATIVE.getStart() && rating <= NEGATIVE.getEnd())
      return NEGATIVE;

    if (rating >= MOSTLY_NEGATIVE.getStart() && rating <= MOSTLY_NEGATIVE.getEnd())
      return MOSTLY_NEGATIVE;

    if (rating >= NEUTRAL.getStart() && rating <= NEUTRAL.getEnd())
      return NEUTRAL;

    if (rating >= MOSTLY_POSITIVE.getStart() && rating <= MOSTLY_POSITIVE.getEnd())
      return MOSTLY_POSITIVE;

    return POSITIVE;
  }
}
