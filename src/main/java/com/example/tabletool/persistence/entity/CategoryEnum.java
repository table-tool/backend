package com.example.tabletool.persistence.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.*;

@Schema(description = "Категория игры")
public enum CategoryEnum {

  STRATEGY("strategy", UUID.fromString("97f23a96-bb70-473d-a47e-0caa3a4a667d")),
  PARTYGAME("partygame", UUID.fromString("9c0e4395-063b-4d31-bc68-e63b3e707837")),
  COOPERATIVE("cooperative", UUID.fromString("c4c55ab5-e023-4dbd-8bea-87229455f5a4")),
  ROLEPLAY("roleplay", UUID.fromString("667e8a46-f4c1-44bd-9516-5b3b05bb4dc1")),
  WARGAME("wargame", UUID.fromString("c172aa7c-c992-446c-9984-0faba04b147e")),
  COLLECTIBLE("collectible", UUID.fromString("c975cbdf-7dc9-44a0-b30c-a1088c3ecf12")),
  ;

  private final String name;

  private final UUID uuid;

  CategoryEnum(String name, UUID uuid) {
    this.name = name;
    this.uuid = uuid;
  }

  public static Optional<CategoryEnum> fromName(String name) {
    if (name == null) {
      return Optional.empty();
    }

    for (var value : CategoryEnum.values()) {
      if (Objects.equals(value.name, name)) {
        return Optional.of(value);
      }
    }

    return Optional.empty();
  }

  public static List<CategoryEnum> valuesAsList(){
    return Arrays.asList(CategoryEnum.values());
  }

  @JsonValue
  public String getName() {
    return name;
  }

  public UUID getUuid() {
    return uuid;
  }
}
