package com.example.tabletool;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.example.tabletool")
public class TableToolApplication {

  public static void main(String[] args) {
    SpringApplication.run(TableToolApplication.class, args);
  }
}
